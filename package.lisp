;;;; package.lisp

(defpackage #:apod-background
  (:use #:cl :stumpwm)
  (:export #:apod-show-description
           #:apod-update-background
           #:apod-download-html
           #:apod-get-image
           #:apod-change-wallpaper))
