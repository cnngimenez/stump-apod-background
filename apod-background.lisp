;;;; apod-background.lisp

(in-package #:apod-background)

(defconstant apod-base-url "https://apod.nasa.gov/"
  "Base URL for the NASA APOD.")

(defvar apod-img-link ""
  "The last image link.")

(defvar apod-description ""
  "The last description downloaded from APOD.")

(defun apod-download-html ()
  "Download the HTML and return the interesting data."
  ;; (let* ((str-page (drakma:http-request "https://apod.nasa.gov/"))
  ;;        (document (chtml:parse str-page (cxml-stp:make-builder))))
  ;;   (xpath
  (let* ((request (dex:get apod-base-url))
         (parsed-content (lquery:$ (initialize request))))
    (list (aref (lquery:$ parsed-content "img" (attr :src)) 0) ;; The image link
          ;; The explanation
          (aref (lquery:$ parsed-content "p" (text)) 2))))

(defun apod-save-last-data (downloaded-data)
  (setq apod-img-link (car downloaded-data))
  (setq apod-description (cadr downloaded-data)))

(defun apod-get-image (url-image-suffix)
  "Download the binary data of the image and save it."
  (let ((imagedata (dex:get (concat apod-base-url url-image-suffix))))
    (with-open-file (f "/tmp/apod-background.png"
                       :if-exists :overwrite
                       :direction :output
                       :element-type '(unsigned-byte 8))
      (write-sequence imagedata f))))

(defun apod-change-wallpaper ()
  "Change the wallpaper to the saved."
  (run-shell-command "feh --bg-max /tmp/apod-background.png"))

(defcommand apod-show-description () ()
  (if (> (length apod-description) 250)
      (run-shell-command (format "echo ~S | dzen2 -l 10 -e 'onstart=grabkeys,uncollapse,scrollhome;key_Escape=ungrabkeys,exit;key_Down=scrolldown;key_Up=scrollup;key_End=scrollend;key_Home=scrollhome;key_Page_Up=scrollup:10;key_Page_Down=scrolldown:10' -p -bg black -fg white -xs 2" apod-description))
      ;; (message "~A%" (subseq apod-description 0 250))
      (message "~A%" apod-description)))

(defcommand apod-update-background () ()
  (apod-save-last-data (apod-download-html))
  (if (string= apod-img-link "")
      (message "Image could not be downloaded.")
      (progn (apod-get-image apod-img-link)
             (apod-change-wallpaper)
             (apod-show-description))))


      
