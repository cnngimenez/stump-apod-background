;;;; apod-background.asd

(asdf:defsystem #:apod-background
  :description "Describe apod-background here"
  :author "Your Name <your.name@example.com>"
  :license  "GPLv3"
  :version "0.0.1"
  :serial t
  :depends-on (#:stumpwm
               ;; #:drakma #:closure-html #:cxml-stp #:plexippus-xpath)
               #:dexador #:plump #:lquery #:lparallel)
  :components ((:file "package")
               (:file "apod-background")))
